from core import rot, smoothstep
from base_shader import BaseShader

import taichi as ti
import taichi_glsl as ts
import numpy as np

# Элементы анимации:
#   Диагональные полосы
# Ссылка на graphtoy.com : https://graphtoy.com/?f1(x,t)=fract(x*cos((-1)*PI/1)%20+%200.33*t)&v1=true&f2(x,t)=&v2=false&f3(x,t)=&v3=false&f4(x,t)=&v4=false&f5(x,t)=&v5=false&f6(x,t)=&v6=false&grid=1&coords=-1.525958431472869,0.4031007751937984,2.8581184133333353

#   Затемнение экрана по краям


# Создание данного в варианте (6 вариант) шейдера можно разделить на следующие этапы:
#   1. Сначала будем менять uv-координату пикселя y для достижения эффекта движения вниз
#   2. Сделаем поворот координат на -pi/4 градуса, чтобы линии были диагональными
#   3. Затем создадим движущиеся полосы за счет функции взятия дробной части fract с аргументом, зависящим от x и t.
#      Цвет каждой полосы определяется на основе ее ширины и положения
#   4. После вычисления цвета пикселя происходит затемнение краев изображения.
#      Затемнение вычисляется на основе расстояния от пикселя до ближайшего края изображения и применяется к цвету пикселя.


@ti.data_oriented
class MyShader(BaseShader):
    @ti.func
    def main_image(self, uv, t):
        '''
        Функция для генерации изображения.
        :param uv: UV-координаты пикселя.
        :param t: Время.
        :return: color (vec3): Цвет пикселя.
        '''
        uv.y -= 0.1 * t  # изменяем у для прокрутки страницы вниз
        uv = rot(-np.pi / 4) @ uv  # поворот координат на 45 градусов
        stripe = ts.fract(uv.x + 0.3 * t)  # движение полос со временем по x

        color = ts.vec3(0.0, 0.0, 0.0)  # цвет полос
        width = 0.125  # ширина каждой полосы

        if stripe < width:
            color = ts.vec3(1.0, 0.0, 0.0)  # красный
        elif stripe < 2 * width:
            color = ts.vec3(1.0, 1.0, 1.0)  # белый
        elif stripe < 3 * width:
            color = ts.vec3(0.0, 0.0, 1.0)  # синий
        elif stripe < 4 * width:
            color = ts.vec3(1.0, 1.0, 1.0)  # снова
        elif stripe < 5 * width:
            color = ts.vec3(1.0, 0.0, 0.0)  # красный
        elif stripe < 6 * width:
            color = ts.vec3(1.0, 1.0, 1.0)  # белый
        elif stripe < 7 * width:
            color = ts.vec3(0.0, 0.0, 1.0)  # синий
        elif stripe < 8 * width:
            color = ts.vec3(1.0, 1.0, 1.0)  # белый

        return color

    @ti.kernel
    def render(self, t: ti.f32):
        '''
        Функция рендеринга
        :param t: Время.
        '''
        for fragCoord in ti.grouped(self.pixels):
            uv = (fragCoord - 0.5 * self.resf) / self.resf.y
            col = self.main_image(uv, t)

            # затемнение относительно координаты x фрагмента
            edge_dist = min(fragCoord.x / self.res[0],
                            1.0 - fragCoord.x / self.res[0])  # нормализованное расстояние до ближайшего края
            fade = smoothstep(0.27, 0.5, edge_dist)  # настраиваем ширину затемнения по краям

            col *= fade  # применяем затемнение

            if self.gamma > 0.0:
                col = ts.clamp(col ** (1 / self.gamma), 0., 1.)
            self.pixels[fragCoord] = col


if __name__ == "__main__":
    ti.init(arch=ti.opengl)
    shader = MyShader("My Shader", res=(1280, 720))
    shader.main_loop()
