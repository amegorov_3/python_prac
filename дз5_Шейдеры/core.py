import taichi as ti
import taichi_glsl as ts


@ti.func
def smoothstep(edge0: float, edge1: float, x: float):
    '''
    Выполняет плавное интерполяционное сглаживание на интервале на основе значения x
    :param edge0: Начальное значение интервала интерполяции.
    :param edge1: Конечное значение интервала интерполяции.
    :param x: Входное значение для интерполяции.
    :return: Результат плавного интерполяционного сглаживания.
    '''
    t = ts.clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
    return t * t * (3.0 - 2.0 * t)


@ti.func
def rot(a):
    '''
    Функция возвращает матрицу поворота координат для угла a
    :param a: угол в радианах
    :return: Матрица поворота 2x2.
    '''
    c = ti.cos(a)
    s = ti.sin(a)
    return ts.mat([c, -s], [s, c])
